#include <iostream>
#include <math.h>
#include "triangulo.hpp"

using namespace std;

tringulo::triangulo(){
    tipo="triangulo"
    base=5.0f;
    altura=2.0f;
}
triangulo::triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
triangulo::~triangulo(){}

float triangulo::calcula_area(){
    return (get_base()*get_altura()) / 2.0;

}
float triangulo::calcula_perimetro(){
    return sqrt(pow(get_base(),2) + pow(get_altura(),2)) + get_base() + get_altura();
}
