#include <iostream>
#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado() {
    set_tipo("Quadrado");
    set_base(2.0f);
    set_altura(2.0f);
}

Quadrado::Quadrado(float base, float altura) {
    set_tipo("Quadrado");
    set_base(base);
    set_altura(altura);
}

Quadrado::Quadrado(string tipo, float base, float altura) {
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

void Quadrado::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Base: " << get_base() << endl;
    cout << "Altura: " << get_altura() << endl;
    cout << "Área: " << calcula_area() << endl;
    cout << "Perímetro: " << calcula_perimetro() << endl;
}