#include <iostream>
#include "circulo.hpp"

using namespace std;

circulo::circulo(){
    tipo="circulo"
    raio= 10.0f

}

circulo::circulo(float raio){
    tipo="circulo"
    set_raio(raio);

}
circulo::circulo(string tipo, float raio){
    set_tipo(tipo);
    set_raio(raio);

}
circulo::~circulo(){}

void circulo::set_raio(float raio){
    this->raio=raio;

}
float circulo::get_raio(){
    return raio;
}
float circulo::calcula_area(){
    return 3.14f * (get_raio() *  get_raio());    
}
float circulo::calcula_perimetro(){
    return 2.0 * 3.14f * get_raio();

}
void Circulo::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Raio: " << get_raio() << endl;
    cout << "Área: " << calcula_area() << endl;
    cout << "Perímetro: " << calcula_perimetro() << endl;
 }


