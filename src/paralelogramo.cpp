#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
	set_tipo("Paralelogramo");
	set_base(1.0f);
	set_altura(4.0f);
}


Paralelogramo::Paralelogramo(float base, float altura){
	set_tipo("Paralelogramo");
	set_base(base);
	set_altura(altura);
}


Paralelogramo::Paralelogramo(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


Paralelogramo::~Paralelogramo(){}

float Paralelogramo::calcula_area(){
	return get_base() * get_altura();
}

float Paralelogramo::calcula_perimetro(){
	return (get_base() + get_altura()) * 2;
}
