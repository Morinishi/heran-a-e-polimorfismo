#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica {
    
private:
    string tipo;
    float lado;
    float apotema;
        
public:
//Métodos construtores
    Pentagono();
    Pentagono(float lado, float apotema);
    Pentagono(string tipo, float lado, float apotema);
    ~Pentagono();

//Métodos acessores
    void set_tipo(string tipo);
    string get_tipo();
    void set_lado(float lado);
    float get_lado();
    void set_apotema(float apotema);
    float get_apotema();

//Demais métodos, neste caso utilizado para obter a área e o perimetro
    float calcula_area();
    float calcula_perimetro();
};

#endif
