#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"

using namespace std;

class Circulo : public FormaGeometrica {
private:
    float raio;

public:
//Métodos construtores
    Circulo();
    Circulo(float raio);
    Circulo(string tipo, float raio);
    ~Circulo();

//Métodos acessores
    void set_raio(float raio);
    float get_raio();

//Demais métodos, neste caso utilizado para obter a área e o perimetro
    float calcula_area();
    float calcula_perimetro();
    void imprime_forma();
};

#endif
