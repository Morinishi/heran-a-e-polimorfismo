#ifndef QUADRADO_HPP
#define QUADRADO_HPP
#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica {
private:
string tipo;
float base;
float altura;
        
public:
 //Métodos construtores
Quadrado();
Quadrado(float base, float altura);
Quadrado(string tipo, float base, float altura);
~Quadrado();

 //Demais métodos, neste caso utilizado para obter a area e o perimetro
    float calcula_area();
    float calcula_perimetro();
    void imprime_forma();


};

#endif
