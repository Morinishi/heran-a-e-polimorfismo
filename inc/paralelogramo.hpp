#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica {
private:
    string tipo;
    float base;
    float altura;

public:
//Métodos construtores
    Paralelogramo();
    Paralelogramo(float base, float altura);
    Paralelogramo(string tipo, float base, float altura);
    ~Paralelogramo();

//Demais métodos, neste caso utilizado para obter a area e o perimetro
    float calcula_area();
    float calcula_perimetro();
        
};

#endif
