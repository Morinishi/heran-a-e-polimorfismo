#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica {

private:
    string tipo;
    float lado;

public:
//Métodos construtores
    Hexagono();
    Hexagono(float lado);
    Hexagono(string tipo, float lado);
    ~Hexagono();
//Métodos acessores
    void set_lado(float lado);
    float get_lado();
//Demais métodos, neste caso utilizado para obter a área e o perimetro 
    float calcula_area();
    float calcula_perimetro();
};

#endif
