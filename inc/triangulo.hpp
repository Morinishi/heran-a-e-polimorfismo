#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"

using namespace std;

class Triangulo : public FormaGeometrica {

private:
string tipo;
float base;
float altura;

public:
//Métodos construtores
Triangulo();
Triangulo(float base, float altura);
Triangulo(string tipo, float base, float altura);
~Triangulo();
//Demais métodos, neste caso utilizado para obter a área e o perimetro
float calcula_area();
float calcula_perimetro();
};

#endif
